#include "BroadcastingTopic.h"

std::vector<BroadcastingTopic *> BroadcastingTopic::broadcastingTopics;
bool BroadcastingTopic::registered = false;

bool bchandler(const String &level, const String &value)

{
    Serial << "received message on : " << level << " - " << value << endl;
    BroadcastingTopic *bt = BroadcastingTopic::find(level.c_str());
    if (bt && bt->getCallback())
    {
        Serial << "received message on right topic on : " << level << " payload : " << value << endl;
        bt->getCallback()->callback(value.c_str());
    }
    return true;
}

void BroadcastingTopic::subscribe(MqttMessageListener *callback)
{
    Serial << "subscribe to " << _topic << endl;
    this->_callback = callback;

    if (!registered)
    {
        Homie.setBroadcastHandler(bchandler);
        registered = true;
    }
}