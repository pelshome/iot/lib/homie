#ifndef HomieDHT_h
#define HomieDHT_h

#include <Homie.h>
#include <DHT.h>
#define HUMIDITY_INTERVAL (5000)

class HomieDHT {
public:
    HomieDHT(
            unsigned int pin,
            unsigned int type
    ) : dht(pin, type){

        node = new HomieNode("dht", "DHT", "dht");

    }

    void setup() {
        dht.begin();
        node->advertise("temperature").setName("Temperature").setDatatype("float").setUnit("ºC");
        node->advertise("humidity").setName("Humidity").setDatatype("float").setUnit("%");
    }

    void loopHandler() {
        if (abs(static_cast<long>(millis() - lastSent)) >= HUMIDITY_INTERVAL || lastSent == 0) {
            lastSent = millis();
            float h = dht.readHumidity();
            float t = dht.readTemperature();

            if (isnan(h) || isnan(t)) {
                Homie.getLogger() << "Failed to read from DHT sensor" << endl;
                return;
            }


            Homie.getLogger() << "Temperature: " << t << " °C" << endl;
            Homie.getLogger() << "Humidity: " << h << " %" << endl;
            node->setProperty("temperature").send(String(t));
            node->setProperty("humidity").send(String(h));

        }
    }

private:
    DHT dht;
    HomieNode *node;
    unsigned long lastSent = 0;
};

#endif

