#ifndef HomieButton_h
#define HomieButton_h

#include <Homie.h>
#include <SendingPromise.hpp>
#include <Homie/Datatypes/Interface.hpp>

#define RESEND_INDTERVAL (100)

class HomieButton
{
public:
    HomieButton(unsigned int _pin)
    {
        pin = _pin;
        node = new HomieNode("button", "Button", "button");
    }
    void setup()
    {
        pinMode(pin, INPUT);
        node->advertise("on").setName("On").setDatatype("boolean");
    }

    void loopHandler()
    {

        int status = digitalRead(pin);
        if (status != this->lastState)
        {
            Serial << "button " << status << endl;
            node->setProperty("on").send(String(status));
            this->lastState = status;
        }
    }

private:
    unsigned int pin;
    HomieNode *node;
    int lastState = LOW;
};

#endif