#ifndef HomieBellRing_h
#define HomieBellRing_h

#include <Homie.h>
#include "BroadcastingTopic.h"

#define RING_DURATION (1000)

class HomieBellRing : public MqttMessageListener
{
public:
    HomieBellRing(unsigned int pin) : _pin(pin)
    {
        this->_pin = pin;
    }

    void setup()
    {
        pinMode(_pin, OUTPUT);
        broadcastingTopic = new BroadcastingTopic("bell");
        Homie.getLogger() << "setup bell" << endl;
        broadcastingTopic->subscribe(this);
        
    }

    void loop()
    {

        if (_ringStartAt != 0 && (_ringStartAt + RING_DURATION) < millis())
        {
            digitalWrite(_pin, LOW);
            _ringStartAt = 0;
        }
        if (!_setup)
        {

            _setup = true;
        }
    }

    void callback(const char *payload)
    {

        Serial << "message received 2" << endl;

        Serial << "message received :" << _pin << endl;
        digitalWrite(_pin, HIGH);
        _ringStartAt = millis();
        //digitalWrite(_pin, LOW);

        Serial << "bell ringed " << endl;
    }

private:
    unsigned int _pin;
    BroadcastingTopic *broadcastingTopic;
    bool _setup = false;
    unsigned long _ringStartAt = 0;
};

#endif