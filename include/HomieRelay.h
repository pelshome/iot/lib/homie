#ifndef HomieRelay_h
#define HomieRelay_h

#include <Homie.h>

class HomieRelay
{
public:
    HomieRelay(const char *_id, const char *_name, unsigned int _pin)
    {
        id = _id;
        name = _name;
        pin = _pin;
        node = new HomieNode(id, name, "switch");
    }
    void setup()
    {
        pinMode(pin, OUTPUT);
        digitalWrite(pin, HIGH);
        node->advertise("on").setName("On").setDatatype("boolean").settable([&](const HomieRange &range, const String &value){return handler(range, value);});
    }
    bool handler(const HomieRange &range, const String &value)
    {
        if (value != "true" && value != "false")
            return false;

        bool on = (value == "true");
        digitalWrite(pin, on ? LOW : HIGH);
        node->setProperty("on").send(value);
        Homie.getLogger() << "Relay " << id << " is " << (on ? "on" : "off") << endl;

        return true;
    }

private:
    const char *id;
    const char *name;
    unsigned int pin;
    HomieNode* node;
};

#endif