#pragma once

#include <functional>
#include <Homie.h>
#include <SendingPromise.hpp>
#include <Homie/Datatypes/Interface.hpp>

class MqttMessageListener
{
public:
    virtual void callback(const char *message)
    {
        Serial << "message received" << endl;
    }
};

class BroadcastingTopic
{
public:
    BroadcastingTopic(const char *topicSuffix) : _topicSuffix(topicSuffix)
    {
        _topic = topicSuffix;
        BroadcastingTopic::broadcastingTopics.push_back(this);
    }

    uint16_t sendBroadcast(const String &value)
    {
        if (!HomieInternals::Interface::get().ready)
        {
            HomieInternals::Interface::get().getLogger() << F("✖ setNodeProperty(): impossible now") << endl;
            return 0;
        }
        uint16_t packetId = Homie.getMqttClient().publish(_topic, 1, false, value.c_str());
        return packetId;
    }

    static bool callback(const String &level, String &value)
    {
        Serial << "received message on : " << level << " - " << value << endl;
        BroadcastingTopic *bt = BroadcastingTopic::find(level.c_str());
        if (bt && bt->getCallback())
        {
            Serial << "received message on right topic on : " << level << " payload : " << value << endl;
            bt->getCallback()->callback(value.c_str());
        }
        return true;
    }

    void subscribe(MqttMessageListener *callback);
    

    static BroadcastingTopic *find(const char *id)
    {

        for (BroadcastingTopic *t : BroadcastingTopic::broadcastingTopics)
        {
            if (strcmp(id, t->getTopic()) == 0)
                return t;
        }

        return 0;
    }

    const char *getTopic()
    {
        return _topic;
    }

    MqttMessageListener *getCallback()
    {
        return _callback;
    }

private:
    const char *_topicSuffix;
    const char *_topic;
    MqttMessageListener *_callback;
    static std::vector<BroadcastingTopic *> broadcastingTopics;
    static bool registered;
};
