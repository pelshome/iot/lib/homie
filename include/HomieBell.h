#ifndef HomieBell_h
#define HomieBell_h

#include <Homie.h>
#include <SendingPromise.hpp>
#include <Homie/Datatypes/Interface.hpp>

#define RESEND_INDTERVAL (100)

class HomieBell
{
public:
    HomieBell(unsigned int _pin)
    {
        pin = _pin;
        node = new HomieNode("bell", "Bell", "bell");
    }
    void setup()
    {
        pinMode(pin, INPUT);

        pinMode(D0, OUTPUT);
        node->advertise("on").setName("On").setDatatype("boolean");
    }

    void loopHandler()
    {

        int status = digitalRead(pin);
        
        Serial << "bell status " << status << endl;
        if (status != this->lastState)
        {

            Serial << "bell " << status << endl;
            digitalWrite(D0, !status);
            node->setProperty("on").send(String(status));
            sendBroadcast("bell", String(status));
            this->lastState = status;
        }
    }

    uint16_t sendBroadcast(const String &topicSuffix, const String &value)
    {
        if (!HomieInternals::Interface::get().ready)
        {
            HomieInternals::Interface::get().getLogger() << F("✖ setNodeProperty(): impossible now") << endl;
            return 0;
        }

        char *topic = new char[strlen(HomieInternals::Interface::get().getConfig().get().mqtt.baseTopic) 
            + strlen("&broadcast") + 1 
            + strlen(topicSuffix.c_str()) + 1 + 1]; // last + 6 for range _65536, last + 4 for /set
        strcpy(topic, HomieInternals::Interface::get().getConfig().get().mqtt.baseTopic);
        strcat(topic, "$broadcast");
        strcat_P(topic, PSTR("/"));
        strcat(topic, topicSuffix.c_str());

        uint16_t packetId = Homie.getMqttClient().publish(topic,  0, false, value.c_str());

 
        delete[] topic;

        return packetId;
    }

private:
    unsigned int pin;
    HomieNode *node;
    int lastState = LOW;
};

#endif