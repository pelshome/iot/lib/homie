#ifndef HomieTemperature_h
#define HomieTemperature_h

#include <Homie.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define TEMPERATURE_INTERVAL (5000)

class HomieTemperature
{
public:
    HomieTemperature(unsigned int _pin)
    {
        pin = _pin;
        node = new HomieNode("temperature", "Temperature", "temperature");
        oneWire = new OneWire(pin);
        sensors = new DallasTemperature(oneWire);
    }
    void setup()
    {
        sensors->begin();
        node->advertise("degrees").setName("Degrees").setDatatype("float").setUnit("ºC");
    }

    void loopHandler()
    {
        if (std::abs(static_cast<long>(millis() - lastTemperatureSent)) >= TEMPERATURE_INTERVAL || lastTemperatureSent == 0)
        {
            sensors->requestTemperatures(); 
            float temperature = sensors->getTempCByIndex(0); // Fake temperature here, for the example
            Homie.getLogger() << "Temperature: " << temperature << " °C" << endl;
            node->setProperty("degrees").send(String(temperature));
            lastTemperatureSent = millis();
        }
    }

private:
    unsigned int pin;
    HomieNode *node;
    OneWire *oneWire;
    DallasTemperature *sensors;
    unsigned long lastTemperatureSent = 0;
};

#endif